package me.rainnny.hcf.util

import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemFlag
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta
import org.bukkit.inventory.meta.SkullMeta
import java.util.*


class ItemBuilder {
    private val item: ItemStack

    constructor(material: Material?) : this(material, 1) {}
    constructor(item: ItemStack) {
        this.item = item
    }

    constructor(material: Material?, amount: Int) {
        item = ItemStack(material, amount)
    }

    constructor(material: Material?, amount: Int, data: Short) {
        item = ItemStack(material, amount, data)
    }

    fun setName(name: String?): ItemBuilder {
        val itemMeta: ItemMeta = item.getItemMeta()
        itemMeta.displayName = name
        item.setItemMeta(itemMeta)
        return this
    }

    fun setDurability(dur: Short): ItemBuilder {
        item.setDurability(dur)
        return this
    }

    fun addUnsafeEnchantment(enchantment: Enchantment?, level: Int): ItemBuilder {
        item.addUnsafeEnchantment(enchantment, level)
        return this
    }

    fun addEnchant(enchantment: Enchantment?, level: Int): ItemBuilder {
        val itemMeta: ItemMeta = item.getItemMeta()
        itemMeta.addEnchant(enchantment, level, true)
        item.setItemMeta(itemMeta)
        return this
    }

    fun addEnchantments(enchantments: Map<Enchantment?, Int?>?): ItemBuilder {
        item.addEnchantments(enchantments)
        return this
    }

    fun removeEnchantment(enchantment: Enchantment?): ItemBuilder {
        item.removeEnchantment(enchantment)
        return this
    }

    fun clearEnchantments(): ItemBuilder {
        item.getItemMeta().getEnchants()
            .forEach({ enchantment, integer -> item.getItemMeta().removeEnchant(enchantment) })
        return this
    }

    fun setSkullOwner(playerName: String?): ItemBuilder {
        val itemMeta = item.getItemMeta() as SkullMeta
        itemMeta.owner = playerName
        item.setItemMeta(itemMeta)
        return this
    }

    fun addGlow(): ItemBuilder {
        addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1)
        val itemMeta: ItemMeta = item.getItemMeta()
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS)
        item.setItemMeta(itemMeta)
        return this
    }

    fun setInfinityDurability(): ItemBuilder {
        item.setDurability(Short.MAX_VALUE)
        return this
    }

    fun setUnbreakable(unbreakable: Boolean): ItemBuilder {
        val itemMeta: ItemMeta = item.getItemMeta()
        itemMeta.spigot().isUnbreakable = unbreakable
        item.setItemMeta(itemMeta)
        return this
    }

    fun setLore(vararg lore: String?): ItemBuilder {
        val itemMeta: ItemMeta = item.getItemMeta()
        itemMeta.lore = Arrays.asList(*lore)
        item.setItemMeta(itemMeta)
        return this
    }

    fun setLore(lore: List<String?>?): ItemBuilder {
        val itemMeta: ItemMeta = item.getItemMeta()
        itemMeta.lore = lore
        item.setItemMeta(itemMeta)
        return this
    }

    fun removeLoreLine(line: String): ItemBuilder {
        val itemMeta: ItemMeta = item.getItemMeta()
        val lore: MutableList<String> = ArrayList(itemMeta.lore)
        if (!lore.contains(line)) return this
        lore.remove(line)
        itemMeta.lore = lore
        item.setItemMeta(itemMeta)
        return this
    }

    fun addLoreLine(line: String?): ItemBuilder {
        val itemMeta: ItemMeta = item.getItemMeta()
        var lore: MutableList<String?> = ArrayList()
        if (itemMeta.hasLore()) lore = ArrayList(itemMeta.lore)
        lore.add(line)
        itemMeta.lore = lore
        item.setItemMeta(itemMeta)
        return this
    }

    fun addLoreLine(line: String, pos: Int): ItemBuilder {
        val itemMeta: ItemMeta = item.getItemMeta()
        val lore: MutableList<String> = ArrayList(itemMeta.lore)
        lore[pos] = line
        itemMeta.lore = lore
        item.setItemMeta(itemMeta)
        return this
    }

    fun clearLore(): ItemBuilder {
        val itemMeta: ItemMeta = item.getItemMeta()
        val lore: MutableList<String> = ArrayList(itemMeta.lore)
        lore.clear()
        itemMeta.lore = lore
        item.setItemMeta(itemMeta)
        return this
    }

    fun toItemStack(): ItemStack {
        return item
    }
}