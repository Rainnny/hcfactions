package me.rainnny.hcf.visual

import com.google.common.collect.Iterables
import org.bukkit.Location
import org.bukkit.entity.Player

abstract class BlockFiller {
    abstract fun generate(player: Player, location: Location) : VisualBlockData

    open fun bulkGenerate(player: Player, locations: Iterable<Location>) : ArrayList<VisualBlockData> {
        val data = ArrayList<VisualBlockData>(Iterables.size(locations))
        for (location in locations)
            data.add(generate(player, location))
        return data
    }
}