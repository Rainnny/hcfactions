package me.rainnny.hcf.util

import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.World
import org.bukkit.block.Block
import org.bukkit.entity.Player
import java.lang.Integer.min
import kotlin.math.abs
import kotlin.math.max

class Cuboid : Iterable<Block>, Cloneable {
    private lateinit var world: World
    private var maxX = 0
    private var maxY = 0
    private var maxZ = 0
    private var minX = 0
    private var minY = 0
    private var minZ = 0

    constructor(world: World, maxX: Int, maxY: Int, maxZ: Int, minX: Int, minY: Int, minZ: Int) {
        this.world = world
        this.maxX = maxX
        this.maxY = maxY
        this.maxZ = maxZ
        this.minX = minX
        this.minY = minY
        this.minZ = minZ
    }

    constructor(cuboid: Cuboid) : this(cuboid.world, cuboid.maxX, cuboid.maxY, cuboid.maxZ, cuboid.minX, cuboid.minY, cuboid.minZ)

    constructor(a: Location, b: Location) {
        if (a.world.name != b.world.name)
            return
        world = a.world
        maxX = max(a.blockX, b.blockX)
        maxY = max(a.blockY, b.blockY)
        maxZ = max(a.blockZ, b.blockZ)
        minX = min(a.blockX, b.blockX)
        minY = min(a.blockY, b.blockY)
        minZ = min(a.blockZ, b.blockZ)
    }

    constructor(dataMap: HashMap<String, Any>) {
        world = Bukkit.getWorld(dataMap["world"].toString())
        maxX = dataMap["maxX"] as Int
        maxY = dataMap["maxY"] as Int
        maxZ = dataMap["maxZ"] as Int
        minX = dataMap["minX"] as Int
        minY = dataMap["minY"] as Int
        minZ = dataMap["minZ"] as Int
    }

    constructor(cuboidString: String) {
        if (!cuboidString.contains(";"))
            return
        val split = cuboidString.split(";")
        world = Bukkit.getWorld(split[0])
        maxX = split[1].toInt()
        maxY = split[2].toInt()
        maxZ = split[3].toInt()
        minX = split[4].toInt()
        minY = split[5].toInt()
        minZ = split[6].toInt()
    }

    fun containsPlayer(player: Player) : Boolean {
        return world.name == player.location.world.name
                && minX <= player.location.x && maxX >= player.location.x
                && minY <= player.location.y && maxY >= player.location.y
                && minZ <= player.location.z && maxZ >= player.location.z
    }

    fun containsBlock(block: Block) : Boolean {
        return world.name == block.location.world.name
                && minX <= block.location.x && maxX >= block.location.x
                && minY <= block.location.y && maxY >= block.location.y
                && minZ <= block.location.z && maxZ >= block.location.z
    }

    fun locateBlock(world: World, x: Int, y: Int, z: Int) : Block? {
        if (world != this.world)
            return null
        val iterator = iterator()
        while (iterator.hasNext()) {
            val block = iterator.next()
            if (block.location.blockX == x && block.location.blockY == y && block.location.blockZ == z) {
                return block
            }
        }
        return null
    }

    fun locateBlock(world: World, x: Int, z: Int) : Block? {
        if (world != this.world)
            return null
        val iterator = iterator()
        while (iterator.hasNext()) {
            val block = iterator.next()
            if (block.type != Material.AIR)
                println(block.type.name)

            if (block.location.blockX == x && block.location.blockZ == z) {
                return block
            }
        }
        return null
    }

    fun serialize() : HashMap<String, Any> {
        val dataMap = HashMap<String, Any>()
        dataMap["world"] = world.name
        dataMap["maxX"] = maxX
        dataMap["maxY"] = maxY
        dataMap["maxZ"] = maxZ
        dataMap["minX"] = minX
        dataMap["minY"] = minY
        dataMap["minZ"] = minZ
        return dataMap
    }

    override fun toString(): String {
        return world.name + ";" + maxX + ";" + maxY + ";" + maxZ + ";" + minX + ";" + minY + ";" + minZ
    }

    override fun iterator(): CuboidIterator {
        return CuboidIterator(this)
    }

    override fun clone(): Cuboid {
        return Cuboid(this)
    }

    class CuboidIterator(private val cuboid: Cuboid) : Iterator<Block> {
        private var sizeX = 0
        private var sizeY = 0
        private var sizeZ = 0

        private var x = 0
        private var y = 0
        private var z = 0

        init {
            sizeX = abs(cuboid.maxX - cuboid.minX) + 1
            sizeY = abs(cuboid.maxY - cuboid.minZ) + 1
            sizeZ = abs(cuboid.maxY - cuboid.minZ) + 1
        }

        fun iterateBlocks() : Collection<Block> {
            val blocks = ArrayList<Block>()
            for (x in cuboid.minX..cuboid.maxX) {
                for (y in cuboid.minY..cuboid.maxY) {
                    for (z in cuboid.minZ..cuboid.maxZ) {
                        blocks.add(cuboid.world.getBlockAt(x, y, z))
                    }
                }
            }
            return blocks
        }

        fun cornerBlocks() : Array<Block?> {
            val blocks = arrayOfNulls<Block>(8)
            blocks[0] = cuboid.world.getBlockAt(cuboid.maxX, cuboid.maxY, cuboid.maxZ)
            blocks[1] = cuboid.world.getBlockAt(cuboid.maxX, cuboid.maxY, cuboid.minZ)
            blocks[2] = cuboid.world.getBlockAt(cuboid.maxX, cuboid.minY, cuboid.maxZ)
            blocks[3] = cuboid.world.getBlockAt(cuboid.maxX, cuboid.minY, cuboid.minZ)
            blocks[4] = cuboid.world.getBlockAt(cuboid.minX, cuboid.maxY, cuboid.maxZ)
            blocks[5] = cuboid.world.getBlockAt(cuboid.minX, cuboid.maxY, cuboid.minZ)
            blocks[6] = cuboid.world.getBlockAt(cuboid.minX, cuboid.minY, cuboid.maxZ)
            blocks[7] = cuboid.world.getBlockAt(cuboid.minX, cuboid.minY, cuboid.minZ)
            return blocks
        }

        override fun hasNext(): Boolean {
            return x < sizeX && y < sizeY && z < sizeZ
        }

        override fun next(): Block {
            val block = cuboid.world.getBlockAt(cuboid.minX + x, cuboid.minY + y, cuboid.minZ + z)
            if (++x >= sizeX) {
                x = 0
                if (++y >= sizeY) {
                    y = 0
                    ++z
                }
            }
            return block
        }
    }
}