package me.rainnny.hcf.team

import com.google.common.base.Strings
import me.rainnny.hcf.util.Cuboid
import me.rainnny.hcf.util.TimeUtils
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.OfflinePlayer
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.util.*
import kotlin.collections.ArrayList

open class Team(val uuid: UUID, val owner: UUID?, val name: String, val members: ArrayList<UUID>, val home: Location?, val announcement: String?,
                val balance: Int, val open: Boolean, val dtr: Double, val claims: ArrayList<Cuboid>, val timeCreated: Long) {
    companion object {
        fun createNewTeam(owner: UUID?, name: String) : Team? {
            val foundTeam = TeamManager.getTeam(name)
            if (foundTeam != null)
                return null
            return Team(UUID.randomUUID(), owner, name, if (owner == null) ArrayList() else arrayListOf(owner), null,
                if (owner == null) "A system faction!" else null, 0, false, 1.0, ArrayList(), System.currentTimeMillis())
        }
    }

    fun getDisplayName() : String {
        var color = "§a"
        when (name) {
            "Wilderness" -> color = "§2"
            "Warzone" -> color = "§4"
        }
        return "$color$name"
    }

    fun announce(message: String) {
        getOnlinePlayers().forEach { player -> player.sendMessage(message) }
    }

    fun showInfo(sender: CommandSender) {
        sender.sendMessage("§7§m" + Strings.repeat("-", 35))
        sender.sendMessage("§6$name" + (if (isSystem()) "" else " §7[" + getOnlinePlayers().size + "/" + getPlayers().size + "] §m-§e HQ§7: §f" +
                (if (home == null) "Not Set" else "" + home.x + ", " + home.z)))
        if (announcement != null)
            sender.sendMessage("§7§o$announcement")
        sender.sendMessage("")
        sender.sendMessage("§eAge§7: §f" + TimeUtils.convertString(System.currentTimeMillis() - timeCreated))
        if (!isSystem()) {
            sender.sendMessage("§eLeader§7: §f" + Bukkit.getOfflinePlayer(owner).name)
            sender.sendMessage("§eBalance§7: §a$$balance")
            if (claims.isNotEmpty())
                sender.sendMessage("§eClaims§7: §f${claims.size}")
            sender.sendMessage("§eRegenerating§7: §fN/A")
            sender.sendMessage("§eDTR§7: §f" + (if (dtr <= 0) "§c" else "§a") + dtr + "§7/§a1.0" + (if (dtr <= 0) " §c(RAIDABLE)" else ""))
        }
        sender.sendMessage("§7§m" + Strings.repeat("-", 35))
    }

    fun getPlayers() : ArrayList<OfflinePlayer> {
        val players = ArrayList<OfflinePlayer>()
        members.forEach { member -> players.add(Bukkit.getOfflinePlayer(member)) }
        return players
    }

    fun getOnlinePlayers() : ArrayList<Player> {
        val players = ArrayList<Player>()
        members.map { member -> Bukkit.getPlayer(member) }.filter { player -> player != null }.forEach { player -> players.add(player) }
        return players
    }

    fun getOfflinePlayers() : ArrayList<OfflinePlayer> {
        val players = ArrayList<OfflinePlayer>()
        members.filter { member -> Bukkit.getPlayer(member) == null }.forEach { offline -> players.add(Bukkit.getOfflinePlayer(offline)) }
        return players
    }

    fun isSystem() : Boolean {
        return owner == null && announcement == "A system faction!"
    }

    fun save() {
        TeamManager.teams.add(this)
        val config = TeamManager.file.getConfiguration()
        if (owner != null)
            config.set("teams.$uuid.owner", owner.toString())
        config.set("teams.$uuid.name", name)

        val members = ArrayList<String>()
        for (member in this.members) {
            members.add(member.toString())
        }
        config.set("teams.$uuid.members", members)

        if (home != null) {
            config.set("teams.$uuid.home.world", home.world.name)
            config.set("teams.$uuid.home.x", home.x)
            config.set("teams.$uuid.home.y", home.y)
            config.set("teams.$uuid.home.z", home.z)
            config.set("teams.$uuid.home.yaw", home.yaw)
            config.set("teams.$uuid.home.pitch", home.pitch)
        }

        if (announcement != null)
            config.set("teams.$uuid.announcement", announcement)
        config.set("teams.$uuid.balance", balance)
        config.set("teams.$uuid.open", open)
        config.set("teams.$uuid.dtr", dtr)

        val claimStrings = ArrayList<String>()
        for (cuboid in claims)
            claimStrings.add(cuboid.toString())
        config.set("teams.$uuid.claims", claimStrings)

        config.set("teams.$uuid.timeCreated", timeCreated)
        TeamManager.file.saveConfig()
    }

    fun disband() {
        TeamManager.teams.remove(this)
        TeamManager.file.getConfiguration().set("teams.$uuid", null)
        TeamManager.file.saveConfig()
    }
}