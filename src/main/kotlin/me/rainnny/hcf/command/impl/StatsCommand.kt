package me.rainnny.hcf.command.impl

import me.rainnny.hcf.command.HCFCommand
import me.rainnny.hcf.menu.StatsMenu
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin

class StatsCommand(plugin: JavaPlugin, name: String) : HCFCommand(plugin, name) {
    override fun onCommand(sender: CommandSender, args: Array<out String>) {
        if (sender is ConsoleCommandSender)
            sender.sendMessage("§cOnly players can use this command!")
        else {
            val player = sender as Player
            var target : Player? = player
            if (args.size > 1)
                target = Bukkit.getPlayer(args[0])
            if (target == null)
                player.sendMessage("§cThat player is not online!")
            else StatsMenu(player, target)
        }
    }
}