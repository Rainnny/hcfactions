package me.rainnny.hcf.command

import org.bukkit.command.CommandSender

abstract class CommandArgument(val alias: String, var usage: String = "", val description: String) {
    constructor(alias: String, description: String) : this(alias, "", description)

    abstract fun onCommand(sender: CommandSender, args: Array<out String>) : Boolean
}