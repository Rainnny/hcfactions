package me.rainnny.hcf.util

import org.bukkit.ChatColor

class Color {
    companion object {
        fun translate(s: String) : String {
            return ChatColor.translateAlternateColorCodes('&', s)
        }
    }
}