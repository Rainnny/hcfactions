package me.rainnny.hcf.visual

import org.bukkit.Material
import org.bukkit.material.MaterialData

class VisualBlockData(type: Material?, data: Byte) : MaterialData(type, data)