package me.rainnny.hcf.menu

import fr.minuskube.inv.ClickableItem
import fr.minuskube.inv.SmartInventory
import fr.minuskube.inv.content.InventoryContents
import fr.minuskube.inv.content.InventoryProvider
import me.rainnny.hcf.HCFactions
import me.rainnny.hcf.util.ItemBuilder
import org.apache.commons.lang.WordUtils
import org.bukkit.Material
import org.bukkit.SkullType
import org.bukkit.Statistic
import org.bukkit.entity.Player

class StatsMenu(val player: Player, val target: Player) : InventoryProvider {
    init {
        SmartInventory.builder()
            .id("statsInv")
            .provider(this)
            .manager(HCFactions.inventoryManager)
            .title("${target.name}'s Stats")
            .size(3, 9)
            .build().open(player)
    }

    override fun init(player: Player?, contents: InventoryContents?) {
        if (player == null || contents == null)
            return
        val oreNames = arrayListOf("QUARTZ_ORE", "EMERALD_ORE", "DIAMOND_ORE", "LAPIS_ORE", "GOLD_ORE", "REDSTONE_ORE", "IRON_ORE", "COAL_ORE")
        val ores = HashMap<Material, Int>()

        oreNames.map { oreName -> enumValueOf<Material>(oreName) }.forEach { material ->
            ores[material] = target.getStatistic(Statistic.MINE_BLOCK, material)
        }

        val lore = ArrayList<String>()
        lore.add("")
        lore.add("§eOres")
        for (entry in ores.entries) {
            val color : String? = when (entry.key) {
                Material.EMERALD_ORE -> "§a"
                Material.DIAMOND_ORE -> "§b"
                Material.LAPIS_ORE -> "§9"
                Material.GOLD_ORE -> "§6"
                Material.REDSTONE_ORE -> "§c"
                Material.COAL_ORE -> "§8"
                else -> "§f"
            }
            lore.add(" $color" + WordUtils.capitalize(entry.key.name.split("_")[0].toLowerCase()) + " §f${entry.value}")
        }
        contents.set(0, 4, ClickableItem.empty(ItemBuilder(Material.SKULL_ITEM, 1, SkullType.PLAYER.ordinal.toShort())
            .setSkullOwner(target.name).setName("§6${target.name}'s Stats").setLore(lore).toItemStack()))

        contents.set(2, 3, ClickableItem.empty(ItemBuilder(Material.IRON_SWORD)
            .setName("§cKills §f" + target.getStatistic(Statistic.PLAYER_KILLS)).toItemStack()))

        contents.set(2, 5, ClickableItem.empty(ItemBuilder(Material.BONE)
            .setName("§cDeaths §f" + target.getStatistic(Statistic.DEATHS)).toItemStack()))
    }

    override fun update(player: Player?, contents: InventoryContents?) {}
}