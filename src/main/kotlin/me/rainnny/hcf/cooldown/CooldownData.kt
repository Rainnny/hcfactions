package me.rainnny.hcf.cooldown

import org.bukkit.entity.Player

class CooldownData(val player: Player, val cooldown: Cooldown, private val started: Long,
                   val informExpire: Boolean, val scoreboard: Boolean) {
    fun getTimeLeft() : Long {
        return started + cooldown.cooldown - System.currentTimeMillis()
    }

    fun hasExpired() : Boolean {
        return System.currentTimeMillis() - started >= cooldown.cooldown
    }
}