package me.rainnny.hcf.team.command.arguments

import me.rainnny.hcf.command.CommandArgument
import me.rainnny.hcf.cooldown.CooldownManager
import me.rainnny.hcf.cooldown.impl.TeamCreateCooldown
import me.rainnny.hcf.team.TeamManager
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player

class DisbandArgument : CommandArgument("disband", "Disband your team") {
    override fun onCommand(sender: CommandSender, args: Array<out String>): Boolean {
        if (sender is ConsoleCommandSender)
            sender.sendMessage("§cOnly players can run this command!")
        else {
            if (!CooldownManager.giveCooldown(sender as Player, TeamCreateCooldown::class.java, false, false))
                sender.sendMessage("§cYou must wait before you can do this again!")
            else {
                val teams = TeamManager.getTeamsByOwner(sender.uniqueId)
                if (teams.isEmpty())
                    sender.sendMessage("§cYou are not apart of a team!")
                else {
                    val team = teams[0]
                    team.disband()
                    Bukkit.broadcastMessage("§eThe team §d" + team.name + " §ewas disbanded by §b" + sender.name)
                }
            }
        }
        return false
    }
}