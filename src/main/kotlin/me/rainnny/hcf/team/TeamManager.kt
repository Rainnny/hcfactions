package me.rainnny.hcf.team

import me.rainnny.hcf.config.HCFSettings
import me.rainnny.hcf.team.listeners.ClaimListener
import me.rainnny.hcf.team.listeners.PlayerListener
import me.rainnny.hcf.util.Cuboid
import me.rainnny.hcf.util.RainnnyFile
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.World
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

class TeamManager(plugin: JavaPlugin) {
    companion object {
        lateinit var file : RainnnyFile
        var teams = ArrayList<Team>()

        fun getTeam(name: String) : Team? {
            return if (teams.isEmpty()) null else teams.filter { team -> team.name.equals(name, true) }.getOrNull(0)
        }

        fun getByPlayer(player: Player) : Team? {
            val teams = getTeamsByOwner(player.uniqueId)
            if (teams.isEmpty())
                return null
            return teams[0]
        }

        fun getTeamsByOwner(owner: UUID?) : List<Team> {
            val teams = ArrayList<Team>()
            this.teams.filter { team -> if (owner == null) team.isSystem() else team.owner == owner }
                .forEach { team -> teams.add(team) }
            return teams
        }

        fun getTeamAt(location: Location) : Team? {
            return getTeamAt(location.world, location.blockX, location.blockZ)
        }

        private fun getTeamAt(world: World, x: Int, z: Int) : Team? {
            val team = getClaimAt(world, x, z)
            if (team != null)
                return team.second

            val environment = world.environment
            if (environment == World.Environment.THE_END)
                return getTeam("Warzone")

            var warzoneRadius = HCFSettings.warzoneRadius
            if (environment == World.Environment.NETHER)
                warzoneRadius/= 4
            return if (abs(x) > warzoneRadius || abs(z) > warzoneRadius) getTeam("Wilderness") else getTeam("Warzone")
        }

        fun getClaimAt(location: Location) : Pair<Cuboid, Team>? {
            return getClaimAt(location.world, location.blockX, location.blockZ)
        }

        fun getClaimAt(world: World, x: Int, z: Int) : Pair<Cuboid, Team>? {
            for (team in teams) {
                if (team.claims.isEmpty())
                    continue
                for (claim in team.claims) {
                    if (claim.locateBlock(world, x, z) != null) {
                        return Pair(claim, team)
                    }
                }
            }
            return null
        }
    }

    init {
        file = RainnnyFile(plugin, "teams.yml", null)
        val config = file.getConfiguration()
        val section = config.getConfigurationSection("teams")
        if (section != null) {
            for (key in section.getKeys(false)) {
                val uuid = UUID.fromString(key)

                var owner : UUID? = null
                if (config.get("teams.$key.owner") != null)
                    owner = UUID.fromString(config.getString("teams.$key.owner"))

                val name = config.getString("teams.$key.name")

                val members = ArrayList<UUID>()
                for (memberUUID in config.getStringList("teams.$key.members"))
                    members.add(UUID.fromString(memberUUID))

                var home : Location? = null
                if (config.get("teams.$key.home") != null) {
                    val world : World? = Bukkit.getWorld(config.getString("teams.$key.home.world"))
                    val x : Double = config.getDouble("teams.$key.home.x")
                    val y : Double = config.getDouble("teams.$key.home.y")
                    val z : Double = config.getDouble("teams.$key.home.z")
                    val yaw : Float = config.getFloat("teams.$key.home.yaw")
                    val pitch : Float = config.getFloat("teams.$key.home.pitch")
                    home = Location(world, x, y, z, yaw, pitch)
                }

                var announcement : String? = null
                if (config.get("teams.$key.announcement") != null)
                    announcement = config.getString("teams.$key.announcement")

                val balance = config.getInt("teams.$key.balance")
                val open = config.getBoolean("teams.$key.open")
                val dtr = config.getDouble("teams.$key.dtr")

                val claims = ArrayList<Cuboid>()
                for (claimString in config.getStringList("teams.$key.claims"))
                    claims.add(Cuboid(claimString))

                val timeCreated = config.getLong("teams.$key.timeCreated")
                teams.add(Team(uuid, owner, name, members, home, announcement, balance, open, dtr, claims, timeCreated))
            }
        }

        // Creating the system factions (if it exists, it won't create it)
        val sysFactions = arrayOf("Wilderness", "Warzone", "Spawn", "NorthRoad", "EastRoad", "SouthRoad", "WestRoad")
        sysFactions.forEach { sysFaction -> Team.createNewTeam(null, sysFaction)?.save() }
        println("Teams Loaded: " + teams.size)

        // Registering events
        Bukkit.getPluginManager().registerEvents(PlayerListener(), plugin)
        Bukkit.getPluginManager().registerEvents(ClaimListener(), plugin)
    }
}