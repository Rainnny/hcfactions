package me.rainnny.hcf.team

class TeamSorter : Comparator<Team> {
    override fun compare(a: Team?, b: Team?): Int {
        if (a == null || b == null)
            return 0
        return a.getOnlinePlayers().size.compareTo(b.getOnlinePlayers().size)
    }
}