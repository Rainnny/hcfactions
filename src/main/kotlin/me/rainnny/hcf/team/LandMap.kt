package me.rainnny.hcf.team

import me.rainnny.hcf.util.Cuboid
import me.rainnny.hcf.visual.VisualType
import me.rainnny.hcf.visual.VisualiseHandler
import org.bukkit.Location
import org.bukkit.entity.Player
import kotlin.math.min

class LandMap {
    companion object {
        private const val mapRadiusBlocks = 22
        val showingClaimMap = ArrayList<Player>()

        fun updateMap(player: Player, type: VisualType) : Boolean {
            val location = player.location
            val world = location.world
            val locationX = location.blockX
            val locationZ = location.blockZ

            val minX = locationX - mapRadiusBlocks
            val minZ = locationZ - mapRadiusBlocks
            val maxX = locationX + mapRadiusBlocks
            val maxZ = locationZ + mapRadiusBlocks

            val board = LinkedHashSet<Pair<Cuboid, Team>>()
            for (x in minX..maxX) {
                for (z in minZ..maxZ) {
                    val claim = TeamManager.getClaimAt(world, x, z)
                    if (claim != null)
                        board.add(claim)
                }
            }

            if (board.isEmpty()) {
                player.sendMessage("§cNothing to visualise for §f${type.name.toLowerCase()} §cwithin $mapRadiusBlocks blocks of you")
                return false
            }

            for (claim in board) {
                val maxHeight = min(world.maxHeight, 256)
                val corners = claim.first.iterator().cornerBlocks()
                val shown = ArrayList<Location>(maxHeight * corners.size)
                for (corner in corners) {
                    if (corner == null)
                        continue
                    for (y in 0 until maxHeight)
                        shown.add(world.getBlockAt(corner.location.blockX, y, corner.location.blockZ).location)
                }
                VisualiseHandler.generate(player, shown, type, true)
            }
            return true
        }
    }
}