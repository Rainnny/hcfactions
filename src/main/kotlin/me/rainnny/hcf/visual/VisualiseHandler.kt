package me.rainnny.hcf.visual

import com.google.common.collect.HashBasedTable
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.Player
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap

class VisualiseHandler {
    companion object {
        private val storedVisualises = HashBasedTable.create<UUID, Location, VisualBlock>()

        fun generate(player: Player, locations: Iterable<Location>, type: VisualType, canOverwrite: Boolean) : LinkedHashMap<Location, VisualBlockData> {
            synchronized(storedVisualises) {
                val results = LinkedHashMap<Location, VisualBlockData>()
                val filled = type.getBlockFiller().bulkGenerate(player, locations)

                var count = 0
                for (location in locations) {
                    if (!canOverwrite && storedVisualises.contains(player.uniqueId, location))
                        continue
                    val previousType = location.block.type
                    if (previousType.isSolid || previousType != Material.AIR)
                        continue
                    val blockData = filled[count++]
                    results[location] = blockData
                    player.sendBlockChange(location, blockData.itemType, blockData.data)
                    storedVisualises.put(player.uniqueId, location, VisualBlock(type, blockData, location))
                }

                return results
            }
        }

        fun clearVisualBlocks(player: Player, type: VisualType) : Map<Location, VisualBlock> {
            synchronized(storedVisualises) {
                if (!storedVisualises.containsRow(player.uniqueId))
                    return Collections.emptyMap()
                val results = HashMap<Location, VisualBlock>(storedVisualises.row(player.uniqueId))
                val removed = HashMap<Location, VisualBlock>()
                for (entry in results.entries) {
                    val block = entry.value
                    if (type == block.type) {
                        val location = entry.key
                        if (removed.put(location, block) == null)
                            clearVisualBlock(player, location)
                    }
                }
                return removed
            }
        }

        fun clearVisualBlock(player: Player, location: Location) : Boolean {
            synchronized(storedVisualises) {
                val visualBlock = storedVisualises.remove(player.uniqueId, location)
                if (visualBlock != null) {
                    val block = location.block
                    val blockData = visualBlock.blockData
                    if (blockData.itemType != block.type || blockData.data != block.data)
                        player.sendBlockChange(location, block.type, block.data)
                    return true
                }
            }
            return false
        }
    }
}