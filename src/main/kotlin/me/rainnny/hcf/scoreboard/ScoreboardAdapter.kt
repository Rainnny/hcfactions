package me.rainnny.hcf.scoreboard

import me.tigerhix.lib.scoreboard.ScoreboardLib
import me.tigerhix.lib.scoreboard.type.Scoreboard
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.plugin.java.JavaPlugin

class ScoreboardAdapter(plugin: JavaPlugin) : Listener {
    private val boards = HashMap<Player, Scoreboard>()

    init {
        ScoreboardLib.setPluginInstance(plugin)
        Bukkit.getPluginManager().registerEvents(this, plugin)
        for (player in Bukkit.getOnlinePlayers())
            giveScoreboard(player)
    }

    @EventHandler
    fun onJoin(event: PlayerJoinEvent) {
        giveScoreboard(event.player)
    }

    @EventHandler
    fun onQuit(event: PlayerQuitEvent) {
        val player = event.player
        if (boards.containsKey(player)) {
            val oldBoard = boards[player]
            if (oldBoard != null) {
                oldBoard.deactivate()
                boards.remove(player)
            }
        }
    }

    private fun giveScoreboard(player: Player) {
        val board = ScoreboardLib.createScoreboard(player).setHandler(ScoreboardProvider()).setUpdateInterval(5L)
        board.activate()
        if (boards.containsKey(player)) {
            val oldBoard = boards[player]
            if (oldBoard != null) {
                oldBoard.deactivate()
                boards.remove(player)
            }
        }
        boards[player] = board
    }
}