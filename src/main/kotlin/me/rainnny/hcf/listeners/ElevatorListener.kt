package me.rainnny.hcf.listeners

import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.Sound
import org.bukkit.block.Sign
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.block.SignChangeEvent
import org.bukkit.event.player.PlayerInteractEvent
import kotlin.math.abs

class ElevatorListener : Listener {
    @EventHandler
    fun onSignChange(event: SignChangeEvent) {
        if (event.lines.size < 2 || !event.lines[0].equals("[Elevator]", true))
            return
        when {
            event.lines[1].equals("Up", true) -> {
                event.setLine(0, "§9[Elevator]")
                event.setLine(1, "Up")
                event.player.playSound(event.player.location, Sound.NOTE_PLING, 0.9f, 1f)
            }
            event.lines[1].equals("Down", true) -> {
                event.setLine(0, "§9[Elevator]")
                event.setLine(1, "Down")
                event.player.playSound(event.player.location, Sound.NOTE_PLING, 0.9f, 1f)
            }
            else -> {
                event.setLine(0, "")
                event.setLine(1, "§cInvalid Elevator")
                event.setLine(2, "§cSign!")
                event.setLine(3, "")
            }
        }
    }

    @EventHandler
    fun onSignClick(event: PlayerInteractEvent) {
        if (event.clickedBlock == null || (event.clickedBlock.type == Material.AIR) || event.action != Action.RIGHT_CLICK_BLOCK)
            return
        val block = event.clickedBlock
        val state = block.state
        if (state is Sign) {
            if (ChatColor.stripColor(state.lines[0]) != "[Elevator]")
                return
            val player = event.player
            val xDifference = abs(player.location.x - block.location.x)
            val zDifference = abs(player.location.z - block.location.z)
            if (xDifference > 2.5 || zDifference > 2.5) {
                player.sendMessage("§cYou're too far away from this elevator sign!")
                return
            }
            if (ChatColor.stripColor(state.lines[1]) == "Up")
                teleport(player, true)
            else if (ChatColor.stripColor(state.lines[1]) == "Down")
                teleport(player, false)
        }
    }

    private fun teleport(player: Player, up: Boolean) {
        var i = player.location.y
        while (i < (if (up) 254 else 1)) {
            val materialAbove = Location(player.world, player.location.blockX.toDouble(), i + 1, player.location.blockZ.toDouble()).block.type
            val material = Location(player.world, player.location.blockX.toDouble(), i, player.location.blockZ.toDouble()).block.type
            val materialBelow = Location(player.world, player.location.blockX.toDouble(), i - 1, player.location.blockZ.toDouble()).block.type
            if (material == Material.AIR && materialAbove == Material.AIR && materialBelow.isSolid && !materialBelow.name.contains("SIGN")) {
                player.teleport(Location(player.world, player.location.blockX.toDouble(), i, player.location.blockZ.toDouble(),
                    player.location.yaw, player.location.pitch))
                return
            }
            if (up)
                i++
            else i--
        }
        player.teleport(Location(player.world, player.location.blockX.toDouble(),
            player.world.getHighestBlockYAt(player.location.blockX, player.location.blockZ).toDouble(), player.location.blockZ.toDouble(),
            player.location.yaw, player.location.pitch))
    }
}