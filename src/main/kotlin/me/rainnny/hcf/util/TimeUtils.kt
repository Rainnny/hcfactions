package me.rainnny.hcf.util

import java.util.concurrent.TimeUnit

class TimeUtils {
    companion object {
        fun convertString(time: Long) : String {
            var unit : TimeUnit = TimeUnit.DAYS
            when {
                time < 60000L -> unit = TimeUnit.SECONDS
                time < 3600000L -> unit = TimeUnit.MINUTES
                time < 86400000L -> unit = TimeUnit.HOURS
            }
            val value : Double
            var text : String
            when (unit) {
                TimeUnit.DAYS -> {
                    value = (time / 86400000.0)
                    text = String.format("%.2f Day", value)
                }
                TimeUnit.HOURS -> {
                    value = (time / 3600000.0)
                    text = String.format("%.2f Hour", value)
                }
                TimeUnit.MINUTES -> {
                    value = (time / 60000.0)
                    text = String.format("%.2f Minute", value)
                }
                TimeUnit.SECONDS -> {
                    value = (time / 1000.0)
                    text = String.format("%.2f Second", value)
                }
                else -> {
                    value = time.toDouble()
                    text = String.format("%.2f Millisecond", value)
                }
            }
            if (value != 1.0)
                text+= "s"
            return text
        }
    }
}