package me.rainnny.hcf.team.listeners

import me.rainnny.hcf.util.ItemBuilder
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerQuitEvent

class ClaimListener : Listener {
    companion object {
        val claimWand = ItemBuilder(Material.GOLD_HOE).setName("§6Claim Wand").setLore(
            "§7Use this wand",
            "§7to claim!!"
        ).toItemStack()
    }

    private val firstLocations = HashMap<Player, Location>()
    private val secondLocations = HashMap<Player, Location>()

    @EventHandler
    fun onInteract(event: PlayerInteractEvent) {
        val player = event.player
        val item = event.item
        if (item == null || (item.type == Material.AIR))
            return
        if (!item.isSimilar(claimWand))
            return
        if (event.action == Action.LEFT_CLICK_BLOCK) {
            event.isCancelled = true
            firstLocations[player] = event.clickedBlock.location
            player.sendMessage("§eSet the first location to §f${event.clickedBlock.location.blockX}§7, §f${event.clickedBlock.location.blockZ}")
        } else if (event.action == Action.RIGHT_CLICK_BLOCK) {
            event.isCancelled = true
            secondLocations[player] = event.clickedBlock.location
            player.sendMessage("§eSet the second location to §f${event.clickedBlock.location.blockX}§7, §f${event.clickedBlock.location.blockZ}")
        }
    }

    @EventHandler
    fun onQuit(event: PlayerQuitEvent) {
        val player = event.player
        if (player.inventory.contains(claimWand)) {
            player.inventory.remove(claimWand)
            player.updateInventory()
        }
        firstLocations.remove(player)
        secondLocations.remove(player)
    }
}