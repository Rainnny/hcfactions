package me.rainnny.hcf.team.command.arguments

import me.rainnny.hcf.command.CommandArgument
import me.rainnny.hcf.team.TeamManager
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player

class InfoArgument : CommandArgument("info", "[player:team]", "Show information on a team") {
    override fun onCommand(sender: CommandSender, args: Array<out String>): Boolean {
        if (args.isNotEmpty()) {
            var team = TeamManager.getTeam(args[0])
            if (team == null && (Bukkit.getOfflinePlayer(args[0]) != null)) {
                val teams = TeamManager.getTeamsByOwner(Bukkit.getOfflinePlayer(args[0]).uniqueId)
                if (teams.isNotEmpty())
                    team = teams[0]
            }
            if (team == null)
                sender.sendMessage("§cThat team was not found!")
            else team.showInfo(sender)
        } else {
            val teams =
                TeamManager.getTeamsByOwner(if (sender is ConsoleCommandSender) null else (sender as Player).uniqueId)
            if (teams.isEmpty())
                sender.sendMessage("§cYou are not apart of a team!")
            else {
                if (teams.size == 1) {
                    teams[0].showInfo(sender)
                } else {
                    sender.sendMessage("§7Showing team information for §b" + teams.size + " §7teams...")
                    for (team in teams)
                        team.showInfo(sender)
                }
            }
        }
        return false
    }
}