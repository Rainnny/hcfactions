package me.rainnny.hcf.team.command.arguments

import me.rainnny.hcf.command.CommandArgument
import me.rainnny.hcf.cooldown.CooldownManager
import me.rainnny.hcf.cooldown.impl.TeamCreateCooldown
import me.rainnny.hcf.team.Team
import me.rainnny.hcf.team.TeamManager
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player

class CreateArgument : CommandArgument("create", "<team>", "Create a new team") {
    override fun onCommand(sender: CommandSender, args: Array<out String>) : Boolean {
        if (args.isEmpty())
            return true
        if (sender is Player) {
            if (!CooldownManager.giveCooldown(sender, TeamCreateCooldown::class.java, false, false)) {
                sender.sendMessage("§cYou must wait before you can do this again!")
                return false
            }
            if (TeamManager.getTeamsByOwner(sender.uniqueId).isNotEmpty()) {
                sender.sendMessage("§cYou're already apart of a team!")
                return false
            }
            if (args[0].length < 3) {
                sender.sendMessage("§cThe minimum length for a team name is 3 characters!")
                return false
            }
            if (args[0].length > 16) {
                sender.sendMessage("§cYou cannot have a team name longer than 16 characters!")
                return false
            }
        }
        if (!Regex("^[a-zA-Z]*\$").matches(args[0])) {
            sender.sendMessage("§cYour team name cannot contain any special characters!")
            return false
        }
        val team = Team.createNewTeam(if (sender is ConsoleCommandSender) null else (sender as Player).uniqueId, args[0])
        if (team == null)
            sender.sendMessage("§cThere is already a team with that name!")
        else {
            team.save()
            Bukkit.broadcastMessage("§d" + team.name + " §ewas created by §b" + sender.name)
        }
        return false
    }
}