package me.rainnny.hcf.util

import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.io.InputStreamReader

class RainnnyFile(private val plugin: JavaPlugin, private val configName: String, folderName: String?) {
    private val configurationFile: File =
        File("plugins/" + plugin.name + (if (folderName == null) "" else "/$folderName"), configName)
    private lateinit var configuration: YamlConfiguration

    init {
        saveDefaultConfig()
        reloadConfig()
    }

    fun saveDefaultConfig() {
        if (!configurationFile.exists())
            plugin.saveResource(configName, false)
    }

    fun saveConfig() {
        configuration.save(configurationFile)
    }

    fun getConfiguration() : FileConfiguration {
        return configuration
    }

    fun reloadConfig() {
        configuration = YamlConfiguration.loadConfiguration(configurationFile)
        val inputStream = plugin.getResource(configName)
        if (inputStream != null) {
            configuration.defaults = YamlConfiguration.loadConfiguration(InputStreamReader(inputStream))
        }
    }
}