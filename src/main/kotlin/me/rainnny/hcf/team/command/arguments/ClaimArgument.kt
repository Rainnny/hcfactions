package me.rainnny.hcf.team.command.arguments

import me.rainnny.hcf.command.CommandArgument
import me.rainnny.hcf.team.TeamManager
import me.rainnny.hcf.team.listeners.ClaimListener
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player

class ClaimArgument : CommandArgument("claim", "Claim land for your team!") {
    override fun onCommand(sender: CommandSender, args: Array<out String>): Boolean {
        if (sender is ConsoleCommandSender)
            sender.sendMessage("§cOnly players can run this command!")
        else {
            val team = TeamManager.getByPlayer(sender as Player)
            if (team == null)
                sender.sendMessage("§cYou are not apart of a team!")
            else {
                sender.inventory.addItem(ClaimListener.claimWand)
                sender.updateInventory()
                sender.sendMessage("§aYou were given the claim wand!")
            }
        }
        return false
    }
}