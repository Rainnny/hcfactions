package me.rainnny.hcf.team.command

import me.rainnny.hcf.command.HCFCommand
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

class TeamCommand(plugin: JavaPlugin, name: String) : HCFCommand(plugin, name) {
    override fun onCommand(sender: CommandSender, args: Array<out String>) {}
}