package me.rainnny.hcf.visual

import me.rainnny.hcf.team.TeamManager
import org.bukkit.DyeColor
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.Player


enum class VisualType {
    SPAWN_BORDER {
        private val blockFiller = object : BlockFiller() {
            override fun generate(player: Player, location: Location): VisualBlockData {
                return VisualBlockData(Material.STAINED_GLASS, DyeColor.RED.data)
            }
        }

        override fun getBlockFiller(): BlockFiller {
            return blockFiller
        }
    },

    CLAIM_BORDER {
        private val blockFiller = object : BlockFiller() {
            override fun generate(player: Player, location: Location): VisualBlockData {
                return VisualBlockData(Material.STAINED_GLASS, DyeColor.GREEN.data)
            }
        }

        override fun getBlockFiller(): BlockFiller {
            return blockFiller
        }
    },

    CLAIM_MAP {
        private val blockFiller = object : BlockFiller() {
            private val types = arrayOf(Material.SNOW_BLOCK, Material.SANDSTONE, Material.FURNACE, Material.NETHERRACK, Material.GLOWSTONE, Material.LAPIS_BLOCK,
                Material.NETHER_BRICK, Material.DIAMOND_ORE, Material.COAL_ORE, Material.IRON_ORE, Material.GOLD_ORE, Material.LAPIS_ORE, Material.REDSTONE_ORE)

            private var materialCounter = 0

            override fun generate(player: Player, location: Location): VisualBlockData {
                val y = location.blockY
                if (y == 0 || y % 3 == 0)
                    return VisualBlockData(types[materialCounter], 0)
                val team = TeamManager.getTeamAt(location)
                return VisualBlockData(Material.STAINED_GLASS, DyeColor.BLUE.data)
            }

            override fun bulkGenerate(player: Player, locations: Iterable<Location>): ArrayList<VisualBlockData> {
                val result = super.bulkGenerate(player, locations)
                if (++materialCounter >= types.size)
                    materialCounter = 0
                return result
            }
        }

        override fun getBlockFiller(): BlockFiller {
            return blockFiller
        }
    },

    CREATE_CLAIM_SELECTION {
        private val blockFiller = object : BlockFiller() {
            override fun generate(player: Player, location: Location): VisualBlockData {
                return VisualBlockData(if (location.blockY % 3 != 0) Material.FENCE else Material.NETHER_FENCE, 0)
            }
        }

        override fun getBlockFiller(): BlockFiller {
            return blockFiller
        }
    };

    abstract fun getBlockFiller() : BlockFiller
}