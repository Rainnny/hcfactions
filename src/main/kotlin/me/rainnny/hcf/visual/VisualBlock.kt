package me.rainnny.hcf.visual

import org.bukkit.Location

class VisualBlock(val type: VisualType, val blockData: VisualBlockData, val location: Location)