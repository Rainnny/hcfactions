package me.rainnny.hcf.team.listeners

import me.rainnny.hcf.team.LandMap
import me.rainnny.hcf.team.Team
import me.rainnny.hcf.team.TeamManager
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.event.player.PlayerQuitEvent

class PlayerListener : Listener {
    private val cachedLocations = HashMap<Player, Team>()

    @EventHandler
    fun onJoin(event: PlayerJoinEvent) {
        val player = event.player
        val team = TeamManager.getByPlayer(player) ?: return
        team.announce("§aTeam member §b" + player.name + " §ahas joined!")
        team.showInfo(player)
    }

    @EventHandler
    fun onMove(event: PlayerMoveEvent) {
        val player = event.player
        val standing = TeamManager.getTeamAt(player.location) ?: return
        if (!cachedLocations.containsKey(player)) {
            cachedLocations[player] = standing
            return
        }
        if (cachedLocations[player] != standing) {
            player.sendMessage("§eNow Leaving: §f" + cachedLocations[player]?.getDisplayName())
            player.sendMessage("§eNow Entering: §f" + standing.getDisplayName())
            cachedLocations[player] = standing
        }
    }

    @EventHandler
    fun onQuit(event: PlayerQuitEvent) {
        val player = event.player

        cachedLocations.remove(player)
        LandMap.showingClaimMap.remove(player)

        val team = TeamManager.getByPlayer(player) ?: return
        team.announce("§cTeam member §b" + player.name + " §chas left!")
    }
}