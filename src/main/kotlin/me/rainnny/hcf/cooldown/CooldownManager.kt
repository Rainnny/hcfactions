package me.rainnny.hcf.cooldown

import me.rainnny.hcf.cooldown.impl.TeamCreateCooldown
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.plugin.java.JavaPlugin

class CooldownManager(plugin: JavaPlugin) : Listener {
    companion object {
        private val cooldowns: ArrayList<Cooldown> = ArrayList()
        private val playerCooldowns = ArrayList<CooldownData>()

        fun giveCooldown(player: Player, clazz: Class<*>, informExpire: Boolean, scoreboard: Boolean) : Boolean {
            val cooldown = getCooldown(clazz) ?: return false
            if (hasCooldown(player, cooldown.javaClass))
                return false
            playerCooldowns.add(CooldownData(player, cooldown, System.currentTimeMillis(), informExpire, scoreboard))
            return true
        }

        fun hasCooldown(player: Player, clazz: Class<*>) : Boolean {
            return playerCooldowns.filter { data -> data.player == player && data.cooldown.javaClass == clazz }.getOrNull(0) != null
        }

        fun getCooldowns(player: Player) : List<CooldownData> {
            return playerCooldowns.filter { data -> data.player == player }.toList()
        }

        fun getCooldown(clazz: Class<*>) : Cooldown? {
            return cooldowns.filter { cooldown -> cooldown.javaClass == clazz }.getOrNull(0)
        }
    }

    init {
        cooldowns.add(TeamCreateCooldown())

        Bukkit.getPluginManager().registerEvents(this, plugin)
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, {
            val iterator = playerCooldowns.iterator()
            while (iterator.hasNext()) {
                val cooldown = iterator.next()
                if (cooldown.hasExpired()) {
                    if (cooldown.informExpire)
                        cooldown.player.sendMessage("§eYou can now use §f" + cooldown.cooldown.name + "§e!")
                    iterator.remove()
                }
            }
        }, 0L, 1L)
    }

    @EventHandler
    fun onQuit(event: PlayerQuitEvent) {
        getCooldowns(event.player).forEach { data -> playerCooldowns.remove(data) }
    }
}