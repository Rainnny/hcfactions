package me.rainnny.hcf

import fr.minuskube.inv.InventoryManager
import me.rainnny.hcf.command.impl.StatsCommand
import me.rainnny.hcf.config.HCFSettings
import me.rainnny.hcf.cooldown.CooldownManager
import me.rainnny.hcf.listeners.ElevatorListener
import me.rainnny.hcf.scoreboard.ScoreboardAdapter
import me.rainnny.hcf.team.TeamManager
import me.rainnny.hcf.team.command.*
import me.rainnny.hcf.team.command.arguments.*
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin

class HCFactions : JavaPlugin() {
    companion object {
        lateinit var inventoryManager: InventoryManager
    }

    override fun onEnable() {
        // Managers
        HCFSettings(this)

        inventoryManager = InventoryManager(this)
        inventoryManager.init()

        CooldownManager(this)
        TeamManager(this)
        ScoreboardAdapter(this)

        // Listeners
        Bukkit.getPluginManager().registerEvents(ElevatorListener(), this)

        // Commands
        StatsCommand(this, "stats").init()

        TeamCommand(this, "team")
            .addArgument(CreateArgument())
            .addArgument(DisbandArgument())
            .addArgument(InfoArgument())
            .addArgument(ClaimArgument())
            .addArgument(MapArgument())
            .addArgument(ListArgument())
            .init()
    }
}