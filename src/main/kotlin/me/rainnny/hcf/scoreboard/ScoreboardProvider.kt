package me.rainnny.hcf.scoreboard

import me.rainnny.hcf.config.HCFSettings
import me.rainnny.hcf.cooldown.CooldownManager
import me.rainnny.hcf.util.TimeUtils
import me.tigerhix.lib.scoreboard.common.EntryBuilder
import me.tigerhix.lib.scoreboard.type.Entry
import me.tigerhix.lib.scoreboard.type.ScoreboardHandler
import org.bukkit.Statistic
import org.bukkit.entity.Player

class ScoreboardProvider : ScoreboardHandler {
    override fun getTitle(player: Player?): String {
        return "  §6§lHCFactions §7- §fMap I"
    }

    override fun getEntries(player: Player?): MutableList<Entry> {
        val builder = EntryBuilder()
        builder.next("§7§m----------------")

        if (player != null) {
            val cooldowns = CooldownManager.getCooldowns(player).filter { data -> data.scoreboard }.toList()

            if (HCFSettings.kitMap) {
                builder.next("§4Kills§7: §f" + player.getStatistic(Statistic.PLAYER_KILLS))
                builder.next("§4Deaths§7: §f" + player.getStatistic(Statistic.DEATHS))
                if (cooldowns.isNotEmpty())
                    builder.next("§7§m----------------")
            }

            if (cooldowns.isNotEmpty()) {
                for (cooldown in cooldowns)
                    builder.next("${cooldown.cooldown.name}§7: §f${TimeUtils.convertString(cooldown.getTimeLeft())}")
            }
        }

        builder.next("§7§m----------------")
        return builder.build()
    }
}