package me.rainnny.hcf.command

import com.google.common.base.Strings
import org.apache.commons.lang.WordUtils
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

abstract class HCFCommand(val plugin: JavaPlugin, val name: String, var showHelp: Boolean = true) : CommandExecutor {
    private val arguments = ArrayList<CommandArgument>()

    fun init() {
        val command = plugin.getCommand(name)
        command.executor = this
        command.permission = "hcf.command.$name"
        command.permissionMessage = "§cYou must have permission §f${command.permission} §cto use this command!"
    }

    abstract fun onCommand(sender: CommandSender, args: Array<out String>)

    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>?): Boolean {
        if (sender == null || label == null || args == null)
            return true
        if (args.isNotEmpty()) {
            val argument = arguments.filter { arg -> arg.alias.equals(args[0], true) }.getOrNull(0)
            if (argument != null) {
                val builder = StringBuilder()
                for (i in 1 until args.size)
                    builder.append(args[i]).append(" ")
                val out = builder.toString().substring(0, if (builder.isEmpty()) 0 else builder.toString().length-1)
                if (!argument.onCommand(sender, if (out.isEmpty()) emptyArray() else out.split(" ").toTypedArray()))
                    return true
            }
        }
        if (!showHelp || arguments.isEmpty())
            onCommand(sender, args)
        else {
            sender.sendMessage("§7§m" + Strings.repeat("-", 35))
            sender.sendMessage("§6§l" + WordUtils.capitalize(label.toLowerCase()) + " §7Help")
            sender.sendMessage("")
            for (argument in arguments)
                sender.sendMessage(" §7* §f/$label ${argument.alias} " + (if (argument.usage.isEmpty()) "" else "${argument.usage} ") +
                        "§7${argument.description}")
            sender.sendMessage("§7§m" + Strings.repeat("-", 35))
        }
        return true
    }

    fun addArgument(argument: CommandArgument) : HCFCommand {
        arguments.add(argument)
        return this
    }
}