package me.rainnny.hcf.team.command.arguments

import me.rainnny.hcf.command.CommandArgument
import me.rainnny.hcf.team.LandMap
import me.rainnny.hcf.visual.VisualType
import me.rainnny.hcf.visual.VisualiseHandler
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player

class MapArgument : CommandArgument("map", "<type>", "Show the team map!") {
    override fun onCommand(sender: CommandSender, args: Array<out String>): Boolean {
        if (sender is ConsoleCommandSender)
            sender.sendMessage("§cOnly players can run this command!")
        else {
            var type = VisualType.CLAIM_MAP
            if (args.isNotEmpty()) {
                try {
                    type = enumValueOf(args[0].toUpperCase())
                } catch (ex: Exception) {
                    val builder = StringBuilder()
                    for (visualType in enumValues<VisualType>())
                        builder.append(visualType.name).append(", ")
                    sender.sendMessage("§cTypes: §f" + builder.toString().substring(0, builder.toString().length-2))
                    return true
                }
            }
            val player = sender as Player
            if (!LandMap.showingClaimMap.contains(player) && (LandMap.updateMap(player, type))) {
                LandMap.showingClaimMap.add(player)
                sender.sendMessage("§aClaim pillars are now shown!")
            } else {
                LandMap.showingClaimMap.remove(player)
                VisualiseHandler.clearVisualBlocks(player, type)
                sender.sendMessage("§cClaim pillars are no longer shown!")
            }
        }
        return false
    }
}