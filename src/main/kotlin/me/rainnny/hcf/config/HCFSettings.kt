package me.rainnny.hcf.config

import me.rainnny.hcf.util.RainnnyFile
import org.bukkit.plugin.java.JavaPlugin

class HCFSettings(plugin: JavaPlugin) {
    companion object {
        var kitMap = false
        var warzoneRadius = 800
    }

    init {
        val config = RainnnyFile(plugin, "settings.yml", null).getConfiguration()
        kitMap = config.getBoolean("kit-map")
        warzoneRadius = config.getInt("warzone-radius")
    }
}