package me.rainnny.hcf.team.command.arguments

import com.google.common.base.Strings
import me.rainnny.hcf.command.CommandArgument
import me.rainnny.hcf.team.Team
import me.rainnny.hcf.team.TeamManager
import me.rainnny.hcf.team.TeamSorter
import org.bukkit.command.CommandSender

class ListArgument : CommandArgument("list", "List top 10 teams") {
    override fun onCommand(sender: CommandSender, args: Array<out String>): Boolean {
        sender.sendMessage("§7§m" + Strings.repeat("-", 35))
        sender.sendMessage("§6Top 10 Factions")
        sender.sendMessage("")

        val teams : ArrayList<Team> = ArrayList()
        for (team in TeamManager.teams) {
            if (team.isSystem())
                continue
            teams.add(team)
        }
        teams.sortWith(TeamSorter())

        if (teams.isEmpty())
            sender.sendMessage("§cNo Teams!")
        else {
            var count = 0
            for (team in teams) {
                if (++count > 10)
                    break
                sender.sendMessage(" §f$count. §e" + team.name +
                        " §7[§a${team.getOnlinePlayers().size}§7/${team.getPlayers().size} Online]  §7[" +
                        (if (team.dtr <= 0) "§c" else "§a") + "${team.dtr} §7DTR]")
            }
        }

        sender.sendMessage("§7§m" + Strings.repeat("-", 35))
        return false
    }
}